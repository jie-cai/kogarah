class ChatsController < ApplicationController

	before_action :signed_in_user

	def index
		id = current_user.id
		type = params[:type] ? params[:type] : 'all'
		talkers = Talker.where(user_id: id, active: 1).includes(chat: [latest: [:user]] );
		case type
		when 'unread'
			key = 'chats#index#unread'
			talkers = unread_chat(id, talkers)
		when 'own'
			key = 'chats#index#own'
			talkers = own_chat(id, talkers)
		else
			key = 'chats#index#all'
			talkers = all_chats(id, talkers)
		end
		chats_to_view(talkers, nil)
		gen_links(	get_links("level", key, id), chats_links )
		renderView
	end

	def show
		chat_id = params[:id].to_i
		chat = Chat.find_by(id: chat_id)
		err_msg = nil
		if chat 
			if talker = Talker.find_by(chat_id: chat.id, user_id: current_user.id)
				show_chat(chat, nil)
				talker.touch
			else
				err_msg = "User is not in the chat group"
			end
		else
			err_msg = "No active chat found with id = #{chat_id}"
		end

		if err_msg
			id = current_user.id
			chats_to_view(all_chats(id), {error: err_msg})
			gen_links(	get_links("level", 'chats#index#all', id), chats_links	)
			renderViewWithURL(4,nil)
		else
			renderView
		end
	end

	def new 
	end

	def create
		message = params[:content]
		to = params[:to]
		chat = Chat.create_new(current_user.id, to, content, nil)

	end

	def update
	end

	def destroy
	end

private 

	def chat_html(page)
		case page
		when 'index'
			asset_path("/chats/index.html")
		when 'new'
			asset_path("/chats/new.html")
		when 'show'
			asset_path("/chats/show.html")
		end			
	end

	def all_chats(user_id, base)
		base.paginate(page: params[:page] )
	end

	def unread_chat(user_id, base)
		base.where("talkers.updated_at < chats.updated_at").references(:chat).paginate(page: params[:page])
	end

	def own_chat(user_id, base)
		base.where(chats: {user_id: user_id}).paginate(page: params[:page])
	end

	def chats_to_view(talkers,flashs)
		json = talkers.map{ |t|
			t.chat.to_h( {message: t.chat.latest.to_h(nil), read: t.after?(t.chat.updated_at)} )
		}
		@view = createView("/chats", chat_html("index"), site_title("Chats"), flashs)
		@view.paginates("chats", "/chats", talkers, json)
	end


	def show_chat(chat, flash)
		messages = chat.messages.includes(:user).paginate(page: params[:page])
		json = messages.map{ |m|
			m.to_h(nil)
		}
		path = "/chats/#{chat.id}"
		@view = createView(path, chat_html("show"), site_title("Chat ##{chat.id}"), flash)
		@view.paginates("messages", path, messages, json )
	end


	def chats_links
		get_links("related", "chats", current_user.id)
	end

end
