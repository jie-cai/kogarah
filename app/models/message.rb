class Message < ActiveRecord::Base
	belongs_to :user
	belongs_to :chat

	
	default_scope -> { order('created_at DESC') }
	self.per_page = 8

	validates	:user_id, 	presence: true
	validates	:chat_id, 	presence: true

	
	def delete
		self.active = nil
	end

	def reply(content, extra)
	end

	def to_h(extra)
		{id: id, user: user_id, login: user.login, avatar: user.avatar.url, active: active, content: content, html: html, extra: extra}
	end
end
